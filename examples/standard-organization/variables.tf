variable "access_key" {
  description = "Credentials: AWS access key."
  #  tfsec:ignore:AWS044
  default = "fake"
}

variable "secret_key" {
  description = "Credentials: AWS secret key. Pass this a variable, never write password in the code."
  # tfsec:ignore:GEN001
  default = "fake"
}
