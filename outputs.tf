output "arn" {
  description = "ARN of the organization."
  value       = aws_organizations_organization.this.arn
}

output "id" {
  description = "Id of the organization."
  value       = aws_organizations_organization.this.id
}

output "account_ids" {
  description = "Map of accounts with their respective ID."
  value = {
    for key, value in aws_organizations_account.this : key => value.id
  }
}

output "account_arns" {
  description = "Map of accounts with their respective ARN."
  value = {
    for key, value in aws_organizations_account.this : key => value.arn
  }
}

output "organizational_unit_ids" {
  description = "Map of OU's with their respective ID."
  value = {
    for key, value in aws_organizations_organizational_unit.this : key => value.id
  }
}

output "organizational_unit_arns" {
  description = "Map of OU's with their respective ARN."
  value = {
    for key, value in aws_organizations_organizational_unit.this : key => value.arn
  }
}
